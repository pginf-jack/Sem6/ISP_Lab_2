library IEEE;
use IEEE.std_logic_1164.all;

--  A testbench has no ports.
entity top_tb is
end top_tb;

architecture behav of top_tb is
  --  Declaration of the component that will be instantiated.
  component top
    port (
      clk_i : in STD_LOGIC;
      rst_i : in STD_LOGIC;
      led_o : out STD_LOGIC_VECTOR (2 downto 0)
    );
  end component;

  --  Specifies which entity is bound with the component.
  for top_0: top use entity work.top;
  signal clk_i : STD_LOGIC := '1';
  signal rst_i : STD_LOGIC := '1';
  signal led_o : STD_LOGIC_VECTOR (2 downto 0);

  constant PERIOD : time := 10 ns;
begin
  --  Component instantiation.
  top_0: top port map (clk_i => clk_i, rst_i => rst_i, led_o => led_o);

  process
    type values_array is array (natural range <>) of STD_LOGIC_VECTOR (2 downto 0);
    constant values: values_array :=
      ("001", "011", "010", "110", "111", "101", "100", "000");
  begin
    -- wait for the reset
    wait for PERIOD/2;
    assert led_o = "000" report "bad led_o value";

    clk_i <= '0';
    rst_i <= '0';
    wait for PERIOD/2;

    -- iterate over half of the expected outputs
    for i in 0 to values'length / 2 loop
      clk_i <= '1';
      wait for PERIOD/2;
      clk_i <= '0';
      wait for PERIOD/2;

      -- check the output
      assert led_o = values(i) report "bad led_o value";
    end loop;

    -- trigger asynchronous reset
    clk_i <= '1';
    wait for 1 ns;
    rst_i <= '1';
    wait for 1 ns;
    rst_i <= '0';

    wait for PERIOD/2 - 2 ns;
    clk_i <= '0';
    wait for PERIOD/2;

    -- iterate over all of the expected outputs, from the beginning
    for i in values'range loop
      clk_i <= '1';
      wait for PERIOD/2;
      clk_i <= '0';
      wait for PERIOD/2;

      -- check the output
      assert led_o = values(i) report "bad led_o value";
    end loop;
    assert false report "end of test" severity note;
    wait;
  end process;
end behav;

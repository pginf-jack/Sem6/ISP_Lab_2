library IEEE;
use IEEE.std_logic_1164.all;

entity top is
    port (
        clk_i : in STD_LOGIC;
        rst_i : in STD_LOGIC;
        led_o : out STD_LOGIC_VECTOR (2 downto 0)
    );
end top;

architecture data_flow of top is
begin
    process (clk_i, rst_i) is
    begin
        if rst_i = '1' then
            led_o <= "000";
        elsif rising_edge(clk_i) then
            case led_o is
                when "000" => led_o <= "001";
                when "001" => led_o <= "011";
                when "011" => led_o <= "010";
                when "010" => led_o <= "110";
                when "110" => led_o <= "111";
                when "111" => led_o <= "101";
                when "101" => led_o <= "100";
                when "100" => led_o <= "000";
                when others => led_o <= "000";
            end case;
        end if;
    end process;
end architecture data_flow;
